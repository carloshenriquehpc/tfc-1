package com.example.tfc1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var etxtTask: EditText? = null
    private var txtTasks: TextView? = null
    private var switchUrg: Switch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.etxtTask  = findViewById(R.id.etxtTask)
        this.txtTasks  = findViewById(R.id.txtTasks)
        this.switchUrg = findViewById(R.id.switchUrg)
    }

    fun onClickOk(v: View) {
        val message: String = this.etxtTask?.text.toString()
        if(message.isNotEmpty()){
            if(switchUrg?.isChecked == true){
                this.txtTasks?.text = this.txtTasks?.text.toString() + "Urgente: " + message + "\n"
                this.etxtTask?.setText("")
            }else{
                this.txtTasks?.text = this.txtTasks?.text.toString() + "Não Urgente: " + message + "\n"
                this.etxtTask?.setText("")
            }

        }
    }
}